class: inverse
layout: true
---
class: center, middle

# An introduction to Chrome Apps

![Default-aligned image](img/chrome-logo.png)

---
class: center, middle

# What Are Chrome Apps?

---

# What Are Chrome Apps?

* Applications written in HTML5, JavaScript and CSS

* Installed using the Chrome browser

* Deliver rich experience like native apps, but as safe as web pages

* Look and behave like native apps, and they have native-like capabilities

* More powerful than web apps, you can interact with file system, network and hardware devices, media tools and much more

* Can be launched from the Chrome browser or externally using the Chrome App Launcher 

* Can be debugged using Chrome Developer Tools like web apps

---
class: center, middle

# Why Writing Chrome Apps?

---

# Why Writing Chrome Apps?

* JavaScript | HTML5 | CSS3

* They are fun, easy to write and debug

* Truly Cross Platform (V8 JavaScript Engine, same as Node.js)

* Easy Support for Offline mode

* Apps are installed on your Google Profile

* Automatically updated

* Share your apps on the [Chrome Web Store](https://chrome.google.com/webstore/category/apps)

---

# How do they look?

* How they look inside is up to the developers
* No address bar or other browser interface elements
* Support for OS window frame or custom frame

--

.image-container[
    ![Default-aligned image](img/frame-os-win.png)
    ![Default-aligned image](img/frame-os-apple.png)
    ![Default-aligned image](img/frame-custom.png)
]

---

# How do they work?

* Chrome Apps integrate closely with a user’s operating system
* They are designed to be run by the same engine as the Chrome browser but outside the browser tab
* They run offline and online

--

##Architecture
![Default-aligned image](img/app-architecture.png)



---
class: center, middle

# How to create a Chrome App?

---

# Create a Manifest file (manifest.json)

A manifest file is used during the installation of the app and on startup.

It contains:
* App Name and icons
* Permission settings
* Background scripts

```json
{
  "version": "1",
  "name": "My First Chrome App",
  "icons": {
    "16": "icon_16.png"
  },
  "app": {
    "background": {
      "scripts": ["background.js"]
    }
  },
  "permissions": [
    "clipboardRead",
    "clipboardWrite",
    "http://www.remote-site.com/*"
  ]
}

```

---

# Background scripts

These are pure JavaScript files which are loaded when the app starts up.

Most of the time your app will have a single background script that listens to the `app.runtime.onLaunched` event:

```javascript
chrome.app.runtime.onLaunched.addListener(function() {

    // do something (e.g. open a page)

}):

```


---

# Pages

These are HTML files which can be launched by other pages or background scripts.

Example to open a page:

```javascript
chrome.app.window.create('main.html', {
    id: 'MyWindowID',
    width: 800,
    minWidth: 800,
    maxWidth: 800,
    height: 600,
    minHeight: 600,
    maxHeight: 600
});

```


---

# Disabled Web Features

`alert` Use a custom lightbox/popup.

`confirm` Use a custom lightbox/popup.

`document.cookie` Packaged app pages are not rendered on the server, so there is no need to use these.

`External resources` Use the webview tag for iframes or blob: URLs for images.

`Form submission` Use JavaScript to process form content (listen for submit event, process data locally first before sending to server).

`JavaScript: URLs` You cannot use bookmarklets for inline JavaScript on anchors. Use the traditional click handler instead.

`Navigation` Links open up with the system web browser. window.history and window.location are disabled.

`Synchronous XMLHttpRequest` Use async-only XMLHttpRequest only.


---
class: center, middle

# {Demo}


---
class: full-image-slide

![image](img/thats-all-folks.jpg)

.footnote[
[https://developer.chrome.com/apps/](https://developer.chrome.com/apps/)
]