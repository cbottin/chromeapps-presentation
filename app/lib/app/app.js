define(function (require) {

    'use strict';

    var auth = require('./auth');
    var LoadingMaskClass = require('./loadingMask');
    var login = require('./login');
    var userData = require('./userData');
    var pullRequests = require('./pullRequests');
    var alert = require('./alert');

    var loadingMask = new LoadingMaskClass(document.body);

    return {
        init: function () {

            login.show(function (username, password) {

                loadingMask.show();

                auth.setAuth(username, password);

                userData.getUserData(function (success, errorDescription) {

                    window.sessionStorage.setItem('username', username);
                    window.sessionStorage.setItem('password', password);

                    loadingMask.hide();

                    if (success) {

                        login.hide();
                        userData.show();
                        pullRequests.show();

                    } else {

                        login.clearPassword();

                        alert('Login Failure', errorDescription, function () {
                           login.focus();
                        });

                    }
                });
            });
        }
    };

});
