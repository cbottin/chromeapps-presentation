define(['jquery'], function ($) {
    
    'use strict';
    
    var DialogModule = function (rootElementSelector) {
        this.rootEl = $(rootElementSelector);
        this.maskEl = $('.dialog-mask');
    };

    DialogModule.prototype = {

        rootEl: null,
        maskEl: null,

        show: function () {
            this.maskEl.addClass('on');
            this.rootEl.addClass('on');
        },

        hide: function () {
            this.maskEl.removeClass('on');
            this.rootEl.removeClass('on');
        }

    };
    
    return DialogModule;
    
});