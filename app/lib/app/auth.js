define(function () {
    
    var auth;
    
    return {
        setAuth: function (username, password) {            
            auth = 'Basic ' + window.btoa(username + ':' + password)
        },
        getAuth: function () {
            return auth;
        }        
    };
    
});