define(['jquery'], function ($) {

    'use strict';

    var callback = {fn: null};
    var rootEl = $('#login');
    var usernameInputEl = rootEl.find('[data-ref=username]');
    var passwordInputEl = rootEl.find('[data-ref=password]');
    var loginButtonEl = rootEl.find('[data-action=login]');

    function loginRequest() {
        var username = usernameInputEl.val();
        var password = passwordInputEl.val();

        chrome.storage.local.set({username: username});
        chrome.storage.local.set({password: password});

        callback(username, password);
    }

    rootEl.on('keydown', function (e) {
        if (e.keyCode === 13) {
            loginRequest();
        }
    });

    loginButtonEl.on('click', function () {
        loginRequest();
    });

    chrome.storage.local.get('username', function (val) {
        usernameInputEl.val(val.username || '');
    });

    chrome.storage.local.get('password', function (val) {
        passwordInputEl.val(val.password || '');
    });

    return {

        show: function (loginRequestCallback, scope) {
            callback = loginRequestCallback;
            rootEl.fadeIn();
        },

        hide: function () {
            rootEl.hide();
        },

        clearPassword: function () {
            passwordInputEl.val('');
        },

        focus: function () {
            passwordInputEl.focus();
        }

    };

});
