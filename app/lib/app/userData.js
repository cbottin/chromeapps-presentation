define(['jquery', './auth'], function ($, auth) {

    'use strict';

    var rootEl = $('#userData');
    var avatarEl = rootEl.find('[data-ref=avatar]');
    var fullnameEl = rootEl.find('[data-ref=fullname]');
    var usernameEl = rootEl.find('[data-ref=username]');

    function getAvatar(avatarUrl, callback, scope) {
        // avatarEl.attr('src', avatarUrl);
        var xhr = new XMLHttpRequest();
        xhr.open('GET', avatarUrl);
        xhr.responseType = 'blob';
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    avatarEl.attr('src', window.URL.createObjectURL(this.response));
                }
                callback.call(scope, true);
            }
        };
        xhr.send();
    }

    function getUserData(callback, scope) {
        $.ajax({
            type: 'GET',
            url: 'https://bitbucket.org/api/2.0/user/',
            headers: {
                'Authorization': auth.getAuth()
            },
            success: function (userData) {

                fullnameEl.text(userData.display_name);
                usernameEl.text(userData.username);

                getAvatar(userData.links.avatar.href, callback, scope);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                callback.call(scope, false, (errorThrown === 'UNAUTHORIZED') ? 'Your credentials appear to be incorrect.' : 'A problem has occurred while making the request');
            }
        });
    }

    return {

        getUserData: getUserData,

        show: function () {
            rootEl.fadeIn();
        },

        hide: function () {
            rootEl.hide();
        }

    };

});
