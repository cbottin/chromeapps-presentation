define(['jquery', 'mustache', './loadingMask', './auth', './alert'], function ($, mustache, LoadingMaskModule, auth, alert) {

    'use strict';

    var rootEl = $('#pullRequests');
    var bodyEl = rootEl.find('.body');
    var tableBody = rootEl.find('tbody');
    var rowTemplate = rootEl.find('.row-template').html();
    var prCountEl = rootEl.find('.pr-count');
    var refreshButtonEl = rootEl.find('[data-action=refresh]');
    var loadingMask = new LoadingMaskModule(bodyEl);
    var refreshInterval = (5 * 60 * 1000); // 5 minutes
    var refreshIntervalId;
    var refreshPullRequests;
    var enabled = false;

    function fillHeight() {
        var bodyOffset = bodyEl.offset();
        var windowHeight = $(window).outerHeight();

        bodyEl.css('height', windowHeight - bodyOffset.top);
    }

    function triggerRefreshInterval() {
        refreshIntervalId = setTimeout(function () {
            refreshPullRequests();
        }, refreshInterval);
    }

    function parsePullRequestPage(html) {

        html = html.replace(/\ssrc=/g, ' data-src='); // prevents from loading the images

        var data = $(html);
        var items = data.find('.iterable-item');
        var pullRequests = [];
        var nextPageLink = data.find('#next-page-link');
        var nextPage = null;

        if (nextPageLink.attr('aria-disabled') !== 'true') {
            const pageLinkParam = 'page=';
            const nextPageLinkHref = nextPageLink.attr('href');
            nextPage = nextPageLinkHref.substring(nextPageLinkHref.lastIndexOf(pageLinkParam) + pageLinkParam.length);
        }

        items.each(function (i, node) {

            var item = $(node);

            var titleLink = item.find('td.title a.execute')

            var pullRequest = {
                repository: item.find('.repo > div > a > span').text(),
                author: item.find('.user > div > span > span').text(),
                link: 'https://bitbucket.org' + titleLink.attr('href'),
                title: titleLink.attr('title'),
                approved: item.find('.approval-link').hasClass('approved'),
                lastUpdate: item.find('.date > div > time').text()
            };

            pullRequests.push(pullRequest);
        });

        var pullRequestsWaitingForApproval = pullRequests.filter(function (pr) {
            return (pr.approved === false);
        });

        return {
            nextPage: nextPage,
            pullRequests: pullRequestsWaitingForApproval
        };
    }

    refreshPullRequests = function (clearList) {

        clearTimeout(refreshIntervalId);

        loadingMask.show();

        if (clearList) {
            tableBody.empty();
            prCountEl.css('visibility', 'hidden');
        }

        getPullRequestPage(1)
            .then((pullRequestsWaitingForApproval) => {
                if (!enabled) return;
                prCountEl.text(pullRequestsWaitingForApproval.length);
                prCountEl.css('visibility', 'visible');
                tableBody.html(mustache.render(rowTemplate, {items: pullRequestsWaitingForApproval}));

                triggerRefreshInterval();

                loadingMask.hide();
            })
            .catch((reason) => {
                if (reason === 'notEnabled') return;
                loadingMask.hide();
                enabled = false;
                alert('The server request failed', 'Press OK to try again.', function () {
                    enabled = true;
                    refreshPullRequests();
                });
            });
    };

    function getPullRequestPage(pageNumber, pullRequests) {
        if (!enabled) {
            return Promise.reject({reason: 'notEnabled'});
        }

        pullRequests = pullRequests || [];
        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'GET',
                url: `https://bitbucket.org/dashboard/pullrequests?section=reviewing&page=${pageNumber}`,
                headers: {
                    'Authorization': auth.getAuth(),
                    'X-PJAX': true,
                    'X-PJAX-Container': '#dashboard-container'
                },
                success: function (html) {
                    const pageResult = parsePullRequestPage(html);
                    pullRequests = pullRequests.concat(pageResult.pullRequests);
                    if (pageResult.nextPage) {
                        getPullRequestPage(pageResult.nextPage, pullRequests)
                            .then((pullRequests2) => resolve(pullRequests2))
                            .catch(() => reject());
                    } else {
                        resolve(pullRequests);
                    }
                },
                error: function () {
                    reject();
                }
            });
        });
    }

    refreshButtonEl.on('click', function () {
        refreshPullRequests();
    });

    $(window).on('resize', function () {
        fillHeight();
    });

    $(document.body).on('keydown', function (e) {
        // F5 key
        if (e.keyCode === 116 && enabled) {
            refreshPullRequests();
        }
    });

    return {

        show: function () {
            enabled = true;
            rootEl.fadeIn();
            fillHeight();
            refreshPullRequests(true);
        },

        hide: function () {
            enabled = false;
            rootEl.hide();
        }

    };

});
