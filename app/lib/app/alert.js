define(['jquery', './dialog'], function ($, DialogModule) {
    
    'use strict';
    
    var rootEl = $('[data-ref=alert-dialog]');
    var dialog = new DialogModule(rootEl);
    var titleEl = rootEl.find('h3');
    var contentEl = rootEl.find('[data-ref=content]');
    var closeButtonEl = rootEl.find('[data-ref=close-button]');
    var callback;

    closeButtonEl.on('click', function () {
        dialog.hide();
        if (callback) {
            callback();
        }
    });
    
    return function (title, message, hideCallback) {

        callback = hideCallback;
        titleEl.text(title);
        contentEl.text(message);

        dialog.show();
        closeButtonEl.focus();
    };
    
});