define(['jquery'], function ($) {
            
    'use strict';
    
    var LoadingMaskgModule = function (targetEl) {
        this.targetEl = $(targetEl);
        this.maskEl = $('<div class="async-spinner"></div>');
        this.maskEl.hide();
        this.maskEl.appendTo(document.body);
    };

    LoadingMaskgModule.prototype = {

        targetEl: null,
        maskEl: null,

        show: function () {
            var targetOffset = this.targetEl.offset();
        
            this.maskEl.css({
                width: this.targetEl.outerWidth(), 
                height: this.targetEl.outerHeight(), 
                left: targetOffset.left, 
                top: targetOffset.top});

            this.maskEl.fadeIn();
        },

        hide: function () {
            this.maskEl.hide();
        }

    };
    
    return LoadingMaskgModule;
    
});